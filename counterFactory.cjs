module.exports = function counterFactory() {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.

    //initialise a counter
    let count = 0;

    //Declaring the function increment to increase the count by 1 whenever called by the object
    function increment(){
        count+=1;
        return count;
    }

    //Declaring the function decrement to decrease the count by 1 whenever called by the object
    function decrement(){
        count-=1;
        return count;
    }

    //returning an object that contains the methods increment and decrement
    return { increment, decrement }
}