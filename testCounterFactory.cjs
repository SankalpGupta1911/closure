//The Test file for counterFactory.cjs

const counterFactory = require("./counterFactory.cjs");  //Importing the function counterFactory from counterFactory.cjs
const tempobject = counterFactory()  //Initialising an object tempobject with the methods: Increment & Decrement.

console.log(tempobject.increment())  //Calling Increment to perform an increment on the count variable 
console.log(tempobject.increment())
console.log(tempobject.increment())
console.log(tempobject.increment())
console.log(tempobject.increment())
console.log(tempobject.increment())

console.log(tempobject.decrement())   //Calling Decrement to perform an decrement on the count variable 
console.log(tempobject.decrement())
console.log(tempobject.decrement())

console.log(tempobject.increment())  //Calling Increment and Decrement to perform the respective operation on the count variable 
console.log(tempobject.decrement())
console.log(tempobject.increment())
console.log(tempobject.decrement())
console.log(tempobject.increment())
console.log(tempobject.decrement())
console.log(tempobject.increment())
console.log(tempobject.decrement())