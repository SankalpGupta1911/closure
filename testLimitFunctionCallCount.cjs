//Test file for limitFunctionCallCount.cjs

const limitFunctionCallCount = require("./limitFunctionCallCount.cjs") //importing the function limitFunctionCallCount
const theInnerFunc = limitFunctionCallCount(printYes,5) /* Initialising theInnerFunc with limitFunctionCallCount with 
                                                            parameters: printYes as cb function 
                                                            and n as the limit for calling the cb function*/

                                                            
function printYes(){ //Initialising the callback function
    return "Yes!"
}

//Calling the function returned to theInnerFunc more than n number of times 
console.log(theInnerFunc())
console.log(theInnerFunc())
console.log(theInnerFunc())
console.log(theInnerFunc())
console.log(theInnerFunc())
console.log(theInnerFunc())
console.log(theInnerFunc())
console.log(theInnerFunc())
console.log(theInnerFunc())
console.log(theInnerFunc())