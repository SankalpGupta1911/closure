//Function Description:
// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned


module.exports = function limitFunctionCallCount(cb, n) { //Exporting the function : limitFunctionCallCount to be imported by test files.

    //Checking if 'n' is not passed or an invalid arguement is passed, if so then return an error prompt
    if(!n || !cb || typeof(n) !== 'number' || typeof(cb)!== 'function') { throw new Error("INVALID INPUT!") }

    let count = 0;

    function funcToCallCb(...arg){

        //Condition to prevent cb from being invoked more than 'n' times.
        if(n>count++){
            return cb(...arg)
        }
        return null
    }

    return funcToCallCb;

}