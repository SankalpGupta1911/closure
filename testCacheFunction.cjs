const cacheFunction = require("./cacheFunction.cjs")

function triple(num) {
    return num * 3;
}

const test = cacheFunction(triple);
console.log(test(12))
console.log(test(13))
console.log(test(14))
console.log(test(15))
console.log(test(12))